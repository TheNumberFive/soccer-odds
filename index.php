<?php

include('simple_html_dom.php');
include('teams.php');

$liga = '3liga';
$ligaRoute = '3-liga';
$saison = '2016-17';
$gameday = 1;
$maxGameday = 1;

while($gameday <= $maxGameday) {
    
    # Use the Curl extension to query Google and get back a page of results
    $url = "http://www.kicker.de/news/fussball/" . $liga . "/spieltag/" . $ligaRoute . "/" . $saison . "/" . $gameday . "/0/spieltag.html";

    $html = file_get_html($url);

    if(!$html) {
        echo 'Site does not exist';
    } else {
        $table = $html->find('table', 0);

        $rows = $table->find("tr[class*=fest]");

        $lastSetDay = null;
        
        echo 'Spieltag ' . $gameday;
        echo '<br/>';
        foreach($rows as $row) {
            $day = $row->find('td', 1);
            $day = explode('&nbsp;', $day);

            if (strlen($day[0]) > 4) {
                $lastSetDay = $day[0];
            }

            $hometeam = $row->find('td', 2);

            $awayteam = $row->find('td', 4);

            $score = $row->find('td', 5);

            $splittedScore = explode('&nbsp;', $score);
            $homeAwayScore = explode(':', $splittedScore[0]);

            echo "Datum: " . $lastSetDay;
            echo "HeimTeam: " . $hometeam->plaintext;
            echo "Asuwaertsteam: " . $awayteam->plaintext;

            echo "Heim Tore: " . $homeAwayScore[0];
            echo "Auswaerts Tore: " . $homeAwayScore[1];

            echo "Ergebnis: " . $score;

            echo "<br/>";
        }
    }
    echo '<br/>';
    echo '<br/>';
    $gameday++;
    
    if($gameday < $maxGameday) {
        sleep(20);   
    }
}

?>