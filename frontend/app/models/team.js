import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  country: DS.attr('string'),
  lookup_name: DS.attr('string')
});
