import Ember from 'ember';

export default Ember.Component.extend({
  options: [],
  selection:'',
  displayKey:'key',
  valueKey: 'value',
  labelName: '',

  init() {
    this._super(...arguments);

    let me = this;
    let displayKey = this.get('displayKey');
    let valueKey = this.get('valueKey');

    Ember.run.scheduleOnce('afterRender', this, function() {
      this.$('select').dropdown({
        onChange: function(value, text, $selectedItem) {
          // custom action
          me.set('selection', me.get('options').findBy(valueKey, value));
        }
      });
    });
  }
});
