import Ember from 'ember';

export default Ember.Component.extend({
  standingsSortProperties: ["points:desc", "tordifferenz:desc", "tore:desc", "gegentore:asc"],
  sortedTeams: Ember.computed.sort("teams", "standingsSortProperties"),
  teams: null
});
