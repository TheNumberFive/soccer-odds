import Ember from 'ember';

export function calcAvg(params/*, hash*/) {
  let [val1, val2, val3, calcOdds] = params;

  if(val1 == null) {
    val1 = 0;
  }

  if(val2 == null) {
    val2 = 0;
  }

  if(val3 == null) {
    val3 = 0;
  }

  let perc = ((val1 + val2 + val3) / 3) * 100;

  perc = perc.toFixed(2);

  if(calcOdds) {
      let val1Perc = val1 * 100;
      let val2Perc = val2 * 100;
      let val3Perc = val3 * 100;

      val1Perc = val1Perc.toFixed(2);
      val2Perc = val2Perc.toFixed(2);
      val3Perc = val3Perc.toFixed(2);

      let val1Odds = 0;
      if(val1Perc != 0) {
        val1Odds = 100 / val1Perc;
      }

      let val2Odds = 0;
      if(val2Perc != 0) {
        val2Odds = 100 / val2Perc;
      }

      let val3Odds = 0;
      if(val3Perc != 0) {
        val3Odds = 100 / val3Perc;
      }

      let odds = (val1Odds + val2Odds + val3Odds) / 3;
      odds = odds.toFixed(2);

      return odds;
  }

  return perc + ' %';
}

export default Ember.Helper.helper(calcAvg);
