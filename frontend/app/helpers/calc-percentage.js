import Ember from 'ember';

export function calcPercentage(params/*, hash*/) {
  let [val1, calcOdds] = params;

  if(val1 == null) {
    val1 = 0;

    return val1;
  }

  let perc = val1 * 100;

  perc = perc.toFixed(2);

  if(calcOdds) {
      let odds = 100 / perc;

      odds = odds.toFixed(2);

      return odds;
  }

  return perc + ' %';
}

export default Ember.Helper.helper(calcPercentage);
