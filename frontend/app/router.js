import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('import');
  this.route('fixture');
  this.route('standing');
  this.route('statistic');
  this.route('season-progress');
});

export default Router;
