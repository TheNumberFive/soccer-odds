import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  model() {
    return RSVP.hash({
      leagues: this.get('store').findAll('league'),
      teams: this.get('store').findAll('team')
    });
  }
});
