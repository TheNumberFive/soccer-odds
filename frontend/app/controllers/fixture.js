import Ember from 'ember';

export default Ember.Controller.extend({
    league: '',
    season: '',
    fixture: null,

    seasons: [{
      id: '2014',
      name: '2014'
    },{
      id: '2015',
      name: '2015'
    },{
      id: '2016',
      name: '2016'
    },{
      id: '2017',
      name: '2017'
    }],

    leagueSelection: null,
    seasonSelection: null,

    isLoading: false,

    actions: {
        getFixture() {
            this.set('isLoading', true);
            let me = this;
            let league = this.get('leagueSelection.id');
            let season = this.get('seasonSelection.id');

            $.ajax({
                url: "/sports-betting/backend/public/get-fixture",
                data: {
                    league: league,
                    season: season
                }
            }).done(function(data) {
                me.set('fixture', data.Fixture);
                me.set('isLoading', false);
            });
        }
    }
});
