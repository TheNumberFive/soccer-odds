import Ember from 'ember';

export default Ember.Controller.extend({
  league: '',
  season: '',
  homeTeam: '',
  awayTeam: '',
  seasonProgress: null,

  seasons: [{
    id: '2014',
    name: '2014'
  },{
    id: '2015',
    name: '2015'
  },{
    id: '2016',
    name: '2016'
  },{
    id: '2017',
    name: '2017'
  }],

  leagueSelection: null,
  seasonSelection: null,
  homeTeamSelection: null,
  awayTeamSelection: null,

  isLoading: false,

  actions: {
    getSeasonProgress() {
        this.set('isLoading', true);
        let me = this;
        let league = this.get('leagueSelection.id');
        let season = this.get('seasonSelection.id');
        let homeTeam = this.get('homeTeamSelection.id');
        let awayTeam = this.get('awayTeamSelection.id');

        $.ajax({
            url: "/sports-betting/backend/public/get-season-progress",
            data: {
                league: league,
                season: season,
                homeTeam: homeTeam,
                awayTeam: awayTeam
            }
        }).done(function(data) {
            me.set('seasonProgress', data.SeasonProgress);
            me.set('isLoading', false);
        });
    }
  }
});
