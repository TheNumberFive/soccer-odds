import Ember from 'ember';

export default Ember.Controller.extend({
  homeTeam: '',
  awayTeam: '',
  stats: null,

  homeTeamSelection: null,
  awayTeamSelection: null,

  isLoading: false,

  actions: {
    getStatistics() {
      this.set('isLoading', true);
      let me = this;
      let homeTeam = this.get('homeTeamSelection.id');
      let awayTeam = this.get('awayTeamSelection.id');

      $.ajax({
          url: "/sports-betting/backend/public/get-statistics",
          data: {
              homeTeam: homeTeam,
              awayTeam: awayTeam
          }
      }).done(function(data) {
          me.set('stats', data.Stats);
          me.set('isLoading', false);
      });
    }
  }
});
