import Ember from 'ember';

export default Ember.Controller.extend({

  league: '',
  season: '',
  participants: null,
  hometable: null,
  awaytable: null,
  totaltable: null,

  seasons: [{
    id: '2014',
    name: '2014'
  },{
    id: '2015',
    name: '2015'
  },{
    id: '2016',
    name: '2016'
  },{
    id: '2017',
    name: '2017'
  }],

  leagueSelection: null,
  seasonSelection: null,

  isLoading: false,

  actions: {
      getStandings() {
        this.set('isLoading', true);
        let me = this;
        let league = this.get('leagueSelection.id');
        let season = this.get('seasonSelection.id');

        $.ajax({
            url: "/sports-betting/backend/public/get-standings",
            data: {
                league: league,
                season: season
            }
        }).done(function(data) {
            me.set('participants', data.Participants);
            me.set('totaltable', data.Total);
            me.set('hometable', data.Home);
            me.set('awaytable', data.Away);

            me.set('isLoading', false);
        });
      }
  }
});
