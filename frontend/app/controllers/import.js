import Ember from 'ember';

export default Ember.Controller.extend({
    league: '',
    leagueRoute: '',
    minGameday: '',
    maxGameday: '',
    seasonFrom: '',
    seasonTo: '',
    crawlUrl: '',

    actions: {
        import() {
            let crawlUrl = this.get('crawlUrl');
            let league = this.get('league');
            let leagueRoute = this.get('leagueRoute');
            let minGameday = this.get('minGameday');
            let maxGameday = this.get('maxGameday');
            let seasonFrom = this.get('seasonFrom');
            let seasonTo = this.get('seasonTo');
            $.ajax({
                url: "/sports-betting/backend/public/import",
                data: {
                    crawlUrl: crawlUrl,
                    league: league,
                    leagueRoute: leagueRoute,
                    minGameday: minGameday,
                    maxGameday: maxGameday,
                    seasonFrom: seasonFrom,
                    seasonTo: seasonTo
                }
            }).done(function() {

            });
        }
    }
});
