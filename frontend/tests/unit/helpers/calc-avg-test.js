import { calcAvg } from 'frontend/helpers/calc-avg';
import { module, test } from 'qunit';

module('Unit | Helper | calc avg');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = calcAvg([42]);
  assert.ok(result);
});
