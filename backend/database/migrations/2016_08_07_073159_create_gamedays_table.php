<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamedaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamedays', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('gameday');
            $table->integer('hometeam_id');
            $table->integer('awayteam_id');
            $table->integer('hometeam_goals');
            $table->integer('awayteam_goals');
            $table->string('winner', 255);
            $table->integer('league_id');
            $table->integer('season');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gamedays');
    }
}
