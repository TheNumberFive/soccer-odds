<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeagueTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_team', function (Blueprint $table) {
            
            $table->integer('league_id')->unsigned();

            $table->foreign('league_id')->references('id')->on('leagues');
            
            $table->integer('team_id')->unsigned();

            $table->foreign('team_id')->references('id')->on('teams');
            
            $table->integer('season');
            
            $table->timestamps();
            
            $table->primary(['league_id', 'team_id', 'season']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('league_team');
    }
}
