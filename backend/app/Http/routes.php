<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('import', 'ImportController@import');
Route::get('get-fixture', 'FixtureController@getFixture');
Route::get('get-season-progress', 'FixtureController@getSeasonProgress');
Route::get('get-standings', 'StandingController@getStandings');
Route::get('get-statistics', 'StatisticController@getStatistics');
Route::get('leagues', 'LeagueController@getLeagues');
Route::get('teams', 'TeamController@getTeams');
