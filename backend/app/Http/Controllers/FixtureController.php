<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;

class FixtureController extends Controller
{
     /**
     * Get Fixture
     *
     * @return Response
     */
    public function getFixture(Request $request) {
        $league = $request->input('league');
        $season = $request->input('season');

        $games = DB::table('gamedays AS g')
            ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
            ->join('teams AS t2', 't2.id', '=', 'g.awayteam_id')
            ->select('g.*', 't1.name AS hometeam_name', 't2.name AS awayteam_name')
            ->where('g.league_id', $league)
            ->where('g.season', $season)
            ->orderBy('g.gameday', 'asc')
            ->get();

        $returnGames = array();
        $gamedayCounter = 1;
        foreach($games as $game) {
            if($game->gameday != $gamedayCounter) {
                $gamedayCounter++;
            }

            $returnGames[$gamedayCounter][] = $game;
        }

        return response()->json(['Fixture' => $returnGames]);
    }

    /**
    * Get Season Progress
    *
    * @return Response
    */
    public function getSeasonProgress(Request $request) {
        $league = $request->input('league');
        $season = $request->input('season');
        $homeTeam = $request->input('homeTeam');
        $awayTeam = $request->input('awayTeam');

        $homeGames = DB::table('gamedays AS g')
            ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
            ->join('teams AS t2', 't2.id', '=', 'g.awayteam_id')
            ->select('g.*', 't1.name AS hometeam_name', 't2.name AS awayteam_name')
            ->where('g.league_id', $league)
            ->where('g.season', $season)
            ->where(function ($query) use($homeTeam) {
                $query->where('g.hometeam_id', $homeTeam)
                      ->orWhere('g.awayteam_id', $homeTeam);
                })
            ->whereNotNull('g.winner')
            ->orderBy('g.date')
            ->get();

        $awayGames = DB::table('gamedays AS g')
            ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
            ->join('teams AS t2', 't2.id', '=', 'g.awayteam_id')
            ->select('g.*', 't1.name AS hometeam_name', 't2.name AS awayteam_name')
            ->where('g.league_id', $league)
            ->where('g.season', $season)
            ->where(function ($query) use($awayTeam) {
                $query->where('g.hometeam_id', $awayTeam)
                      ->orWhere('g.awayteam_id', $awayTeam);
                })
            ->whereNotNull('g.winner')
            ->orderBy('g.date')
            ->get();

        $returnSeasonProgress = (object) array(
          'HomeTeam' => $homeGames,
          'AwayTeam' => $awayGames
        );

        return response()->json(['SeasonProgress' => $returnSeasonProgress]);
    }
}
