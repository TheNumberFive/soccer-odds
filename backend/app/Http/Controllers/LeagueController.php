<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class LeagueController extends Controller
{
  /**
  * Get Leagues
  *
  * @return Response
  */
    public function getLeagues()
    {
      $leagues = DB::table('leagues')->get();

      return response()->json(['Leagues' => $leagues]);
    }
}
