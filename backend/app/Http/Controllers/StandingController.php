<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;

class StandingController extends Controller
{
  /**
  * Get Standings
  *
  * @return Response
  */
  public function getStandings(Request $request) {

     $league = $request->input('league');
     $season = $request->input('season');


     // Participating Teams
     $participants = DB::table('league_team AS lt')
         ->join('teams AS t1', 't1.id', '=', 'lt.team_id')
         ->select('lt.*', 't1.*')
         ->where('lt.league_id', $league)
         ->where('lt.season', $season)
         ->get();


     // Heim Siege
     // alle Teams ein mal (um null auszuschliessen, falls Team nie daheim gewonnen, verloren oder unentschieden gespielt hat)
     $teamsQuery = DB::table('gamedays AS gd')
         ->leftJoin('teams AS t4', 't4.id', '=', 'gd.hometeam_id')
         ->select('gd.*', 't4.name AS hometeam_name')
         ->where('gd.league_id', $league)
         ->where('gd.season', $season)
         ->groupBy('gd.hometeam_id');

     // Left Join Select um die Anzahl der Siege zu bestimmen
     $homeWinsLeftJoin = DB::table('gamedays')
          ->select('hometeam_id', DB::raw('count( hometeam_id ) as heimsiege'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->where('winner', 'home')
          ->groupBy('hometeam_id');

     $homeLossesLeftJoin = DB::table('gamedays')
          ->select('hometeam_id', DB::raw('count( hometeam_id ) as heimniederlagen'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->where('winner', 'away')
          ->groupBy('hometeam_id');

     $homeDrawsLeftJoin = DB::table('gamedays')
          ->select('hometeam_id', DB::raw('count( hometeam_id ) as heimunentschieden'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->where('winner', 'draw')
          ->groupBy('hometeam_id');

     $awayWinsLeftJoin = DB::table('gamedays')
          ->select('awayteam_id', DB::raw('count( awayteam_id ) as auswaertssiege'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->where('winner', 'away')
          ->groupBy('awayteam_id');

     $awayLossesLeftJoin = DB::table('gamedays')
          ->select('awayteam_id', DB::raw('count( awayteam_id ) as auswaertsniederlagen'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->where('winner', 'home')
          ->groupBy('awayteam_id');

     $awayDrawsLeftJoin = DB::table('gamedays')
          ->select('awayteam_id', DB::raw('count( awayteam_id ) as auswaertsunentschieden'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->where('winner', 'draw')
          ->groupBy('awayteam_id');

     $homeGoalsLeftJoin = DB::table('gamedays')
          ->select('hometeam_id', DB::raw('sum( hometeam_goals ) as heimtore'), DB::raw('sum( awayteam_goals ) as heimgegentore'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->groupBy('hometeam_id');

     $awayGoalsLeftJoin = DB::table('gamedays')
          ->select('awayteam_id', DB::raw('sum( awayteam_goals ) as auswaertstore'), DB::raw('sum( hometeam_goals ) as auswaertsgegentore'))
          ->where('league_id', $league)
          ->where('season', $season)
          ->groupBy('awayteam_id');

     // Zusammenführen der einzelnen Statments
     $totalQuery = DB::table( DB::raw("({$teamsQuery->toSql()}) as g") )
        ->select('g.hometeam_id', 'league_id', 'season', 'hometeam_name',
        DB::raw('(IFNULL(heimsiege,0)) as heimsiege'),
        DB::raw('(IFNULL(heimniederlagen,0)) as heimniederlagen'),
        DB::raw('(IFNULL(heimunentschieden,0)) as heimunentschieden'),
        DB::raw('(IFNULL(auswaertssiege,0)) as auswaertssiege'),
        DB::raw('(IFNULL(auswaertsniederlagen,0)) as auswaertsniederlagen'),
        DB::raw('(IFNULL(auswaertsunentschieden,0)) as auswaertsunentschieden'),
        DB::raw('(IFNULL(heimtore,0)) as heimtore'), DB::raw('(IFNULL(heimgegentore,0)) as heimgegentore'),
        DB::raw('(IFNULL(auswaertstore,0)) as auswaertstore'), DB::raw('(IFNULL(auswaertsgegentore,0)) as auswaertsgegentore'),
        DB::raw('(IFNULL(heimsiege,0) + IFNULL(heimniederlagen,0) + IFNULL(heimunentschieden,0)) as heimspiele'),
        DB::raw('(IFNULL(auswaertssiege,0) + IFNULL(auswaertsniederlagen,0) + IFNULL(auswaertsunentschieden,0)) as auswaertsspiele'))
        ->leftJoin(DB::raw("({$homeWinsLeftJoin->toSql()}) as t1"), 'g.hometeam_id', '=', 't1.hometeam_id')
        ->leftJoin(DB::raw("({$homeLossesLeftJoin->toSql()}) as t2"), 'g.hometeam_id', '=', 't2.hometeam_id')
        ->leftJoin(DB::raw("({$homeDrawsLeftJoin->toSql()}) as t3"), 'g.hometeam_id', '=', 't3.hometeam_id')
        ->leftJoin(DB::raw("({$awayWinsLeftJoin->toSql()}) as t6"), 'g.hometeam_id', '=', 't6.awayteam_id')
        ->leftJoin(DB::raw("({$awayLossesLeftJoin->toSql()}) as t7"), 'g.hometeam_id', '=', 't7.awayteam_id')
        ->leftJoin(DB::raw("({$awayDrawsLeftJoin->toSql()}) as t8"), 'g.hometeam_id', '=', 't8.awayteam_id')
        ->leftJoin(DB::raw("({$homeGoalsLeftJoin->toSql()}) as t9"), 'g.hometeam_id', '=', 't9.hometeam_id')
        ->leftJoin(DB::raw("({$awayGoalsLeftJoin->toSql()}) as t10"), 'g.hometeam_id', '=', 't10.awayteam_id');

     // Merge Bindings
     $totalQuery->mergeBindings( $teamsQuery );
     $totalQuery->mergeBindings( $homeWinsLeftJoin );
     $totalQuery->mergeBindings( $homeLossesLeftJoin );
     $totalQuery->mergeBindings( $homeDrawsLeftJoin );
     $totalQuery->mergeBindings( $awayWinsLeftJoin );
     $totalQuery->mergeBindings( $awayLossesLeftJoin );
     $totalQuery->mergeBindings( $awayDrawsLeftJoin );
     $totalQuery->mergeBindings( $homeGoalsLeftJoin );
     $totalQuery->mergeBindings( $awayGoalsLeftJoin );

     // Get
     $total = $totalQuery->get();

     $returnHomeArray = array();
     $returnAwayArray = array();
     $returnTotalArray = array();

     foreach($total as $entry) {
       $homeEntry = (object) array(
         'team_id' => $entry->hometeam_id,
         'siege' => $entry->heimsiege,
         'unentschieden' => $entry->heimunentschieden,
         'niederlagen' => $entry->heimniederlagen,
         'team_name' => $entry->hometeam_name,
         'tore' => (int)$entry->heimtore,
         'gegentore' => (int)$entry->heimgegentore,
         'spiele' => (int)$entry->heimspiele,
         'tordifferenz' => (int)$entry->heimtore - (int)$entry->heimgegentore,
         'points' => 0
       );

       $awayEntry = (object) array(
         'team_id' => $entry->hometeam_id,
         'siege' => $entry->auswaertssiege,
         'unentschieden' => $entry->auswaertsunentschieden,
         'niederlagen' => $entry->auswaertsniederlagen,
         'team_name' => $entry->hometeam_name,
         'tore' => (int)$entry->auswaertstore,
         'gegentore' => (int)$entry->auswaertsgegentore,
         'spiele' => (int)$entry->auswaertsspiele,
         'tordifferenz' => (int)$entry->auswaertstore - (int)$entry->auswaertsgegentore,
         'points' => 0
       );

       $totalEntry = (object) array(
         'team_id' => $entry->hometeam_id,
         'siege' => 0,
         'unentschieden' => 0,
         'niederlagen' => 0,
         'team_name' => $entry->hometeam_name,
         'tore' => 0,
         'gegentore' => 0,
         'spiele' => 0,
         'tordifferenz' => (int)$entry->heimtore + (int)$entry->auswaertstore - (int)$entry->heimgegentore - (int)$entry->auswaertsgegentore,
         'points' => 0
       );

       $homeEntry->points = $homeEntry->siege * 3 + $homeEntry->unentschieden;
       $awayEntry->points = $awayEntry->siege * 3 + $awayEntry->unentschieden;

       $totalEntry->siege = $homeEntry->siege + $awayEntry->siege;
       $totalEntry->unentschieden = $homeEntry->unentschieden + $awayEntry->unentschieden;
       $totalEntry->niederlagen = $homeEntry->niederlagen + $awayEntry->niederlagen;

       $totalEntry->points = $homeEntry->points + $awayEntry->points;
       $totalEntry->tore = $homeEntry->tore + $awayEntry->tore;
       $totalEntry->gegentore = $homeEntry->gegentore + $awayEntry->gegentore;

       $totalEntry->spiele = $homeEntry->spiele + $awayEntry->spiele;

       $returnHomeArray[] = $homeEntry;
       $returnAwayArray[] = $awayEntry;
       $returnTotalArray[] = $totalEntry;
     }

     // Spiele für Gesamt/Heim/Auswärtstabelle
     $games = DB::table('gamedays AS g')
         ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
         ->join('teams AS t2', 't2.id', '=', 'g.awayteam_id')
         ->select('g.*', 't1.name AS hometeam_name', 't2.name AS awayteam_name')
         ->where('g.league_id', $league)
         ->where('g.season', $season)
         ->whereNotNull('g.winner')
         ->get();

      $returnParticipants = array();

      foreach ($participants as $team) {
        $returnParticipants[] = $team;
      }

      return response()->json(['Participants' => $returnParticipants, 'Games' => $games, 'Home' => $returnHomeArray, 'Away' => $returnAwayArray, 'Total' => $returnTotalArray]);
    }
}
