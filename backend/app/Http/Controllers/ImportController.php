<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;

include(app_path() . '\Functions\simple_html_dom.php');

class ImportController extends Controller
{
    /**
     * Import Games
     *
     * @return Response
     */
    public function import(Request $request) {
        $now = date('Y-m-d');

        $league = $request->input('league');
        $leagueRoute = $request->input('leagueRoute');
        $dbSeasonStart = $request->input('seasonFrom');
        $dbSeasonEnd = $request->input('seasonTo');
        $minGameday = $request->input('minGameday');
        $maxGameday = $request->input('maxGameday');
        $crawlUrl = $request->input('crawlUrl');

        $startGameday = $minGameday;

        $season = (string)$dbSeasonStart . '-' . (string)$dbSeasonEnd;

        if(strlen($dbSeasonEnd) == 2) {
            $dbSeasonEnd = '20' . $dbSeasonEnd;
        }

        $teams = DB::table('teams')->get();
        $leagueFromDb = DB::table('leagues')->where('lookup_name', $league)->first();
        /*
        $liga = '3liga';
        $ligaRoute = '3-liga';
        $saison = '2016-17';
        $minGameday = 1;
        $maxGameday = 1;
        */

        $gamedays = array();
        while($minGameday <= $maxGameday) {

            $games = array();
            # Use the Curl extension to query Google and get back a page of results
          //  $url = "http://www.kicker.de/news/fussball/" . $league . "/spieltag/" . $leagueRoute . "/" . $season . "/" . $minGameday . "/0/spieltag.html";
            $url = $crawlUrl . "/" . $leagueRoute . "/" . $season . "/" . $minGameday . "/0/spieltag.html";

            $html = file_get_html($url);

            if(!$html) {
                echo 'Site does not exist';
            } else {
                $table = $html->find('table', 0);

                $rows = $table->find("tr[class*=fest]");

                $lastSetDay = null;

                foreach($rows as $row) {
                   // $game = array();
                    $day = $row->find('td', 1);
                    $day = explode('&nbsp;', $day->plaintext);

                    if (strlen($day[0]) > 4) {
                        $lastSetDay = $day[0];
                    }

                    $hometeam = $row->find('td', 2);

                    $awayteam = $row->find('td', 4);

                    $score = $row->find('td', 5);

                    $splittedScore = explode('&nbsp;', $score->plaintext);
                    $homeAwayScore = explode(':', $splittedScore[0]);

                    $dbHomeTeam = '';
                    $dbHomeTeamId = null;
                    $dbAwayTeam = '';
                    $dbAwayTeamId = null;

                    foreach($teams as $team) {
                        if($team->lookup_name == trim($hometeam->plaintext)) {
                            $dbHomeTeam = $team->name;
                            $dbHomeTeamId = $team->id;
                        }

                        if($team->lookup_name == trim($awayteam->plaintext)) {
                            $dbAwayTeam = $team->name;
                            $dbAwayTeamId = $team->id;
                        }
                    }

                    $trimmedSetDay = trim($lastSetDay);
                    $splittedDate = explode('.', $trimmedSetDay);
                    $day = (int)$splittedDate[0];
                    $month = (int)$splittedDate[1];
                    $year = 0;

                    if($dbSeasonStart != $dbSeasonEnd) {
                        if((int)$month >= 6) {
                            $year = $dbSeasonStart;
                        } else {
                            $year = $dbSeasonEnd;
                        }
                    } else {
                        $year = $dbSeasonStart;
                    }

                    if(strlen($month) == 1) {
                        $month = "0" . (string)$month;
                    }

                    if(strlen($day) == 1) {
                        $day = "0" . (string)$day;
                    }

                    $gameDate = (string)$year . '-' . (string)$month . '-' . (string)$day;

                     $game = array(
                        'Spieltag' => $minGameday,
                        'Datum' => trim($lastSetDay),
                        'HeimTeam' => $dbHomeTeam,
                        'Auswärtsteam' => $dbAwayTeam,
                        'HeimTore' => trim($homeAwayScore[0]),
                        'Auswärtstore' => trim($homeAwayScore[1])
                    );

                    $trimmedHomeGoals = trim($homeAwayScore[0]);
                    if($trimmedHomeGoals == '-') {
                        $trimmedHomeGoals = null;
                    }

                    $trimmedAwayGoals = trim($homeAwayScore[1]);
                    if($trimmedAwayGoals == '-') {
                        $trimmedAwayGoals = null;
                    }

                    $winner = 'NULL';
                    if($trimmedHomeGoals != null && $trimmedAwayGoals != null) {
                        if((int)$trimmedHomeGoals > (int)$trimmedAwayGoals) {
                            $winner = 'home';
                        } else if ((int)$trimmedHomeGoals == (int)$trimmedAwayGoals) {
                            $winner = 'draw';
                        } else if ((int)$trimmedHomeGoals < (int)$trimmedAwayGoals) {
                            $winner = 'away';
                        }
                    }

                    if($winner != 'NULL') {
                        $winner = "'" . $winner . "'";
                    }

                    $sqlHomeGoals = $trimmedHomeGoals;
                    if($trimmedHomeGoals == null) {
                        $sqlHomeGoals = 'NULL';
                    }

                    $sqlAwayGoals = $trimmedAwayGoals;
                    if($trimmedAwayGoals == null) {
                        $sqlAwayGoals = 'NULL';
                    }

                    $gameDate = "'" . $gameDate . "'";
                    $sql[] = "(" . $gameDate . "," . $minGameday . "," . $dbHomeTeamId . "," . $dbAwayTeamId . "," . $sqlHomeGoals . "," . $sqlAwayGoals . "," . $winner . "," . $leagueFromDb->id . "," . $dbSeasonEnd . ")," . "\n";
                    /*
                    array(
                        null, $gameDate, $startGameday, $dbHomeTeamId, $dbAwayTeamId, $trimmedHomeGoals, $trimmedAwayGoals, $winner, 3, $dbSeasonEnd, null, null
                    );
                    */
                    array_push($games, $game);
                }
            }

            array_push($gamedays, $games);

            $minGameday++;

            if($minGameday < $maxGameday) {
                sleep(20);
            }
        }

        $fp = fopen(storage_path() . '/sql/' . $now . '_' . $league . '_' . $season . '_' . $startGameday . '-' . $maxGameday . '.sql', 'w');
        fwrite($fp, "INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES " . "\n");
        foreach ($sql as $line) {
            fwrite($fp, $line);
        }
        fclose($fp);

        return response()->json(['Gamedays' => $gamedays]);
    }
}
