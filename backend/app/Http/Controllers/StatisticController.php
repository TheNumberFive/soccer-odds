<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class StatisticController extends Controller
{
    /**
    * Get Statistics
    *
    * @return Response
    */
    public function getStatistics(Request $request) {

      $homeTeam = $request->input('homeTeam');
      $awayTeam = $request->input('awayTeam');

      $shortHome = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
          ->select('g.*', 't1.name AS hometeam_name')
          ->where('g.hometeam_id', $homeTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(5)
          ->get();

      $midHome = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
          ->select('g.*', 't1.name AS hometeam_name')
          ->where('g.hometeam_id', $homeTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(15)
          ->get();

      $longHome = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
          ->select('g.*', 't1.name AS hometeam_name')
          ->where('g.hometeam_id', $homeTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(25)
          ->get();

      $xtraLongHome = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.hometeam_id')
          ->select('g.*', 't1.name AS hometeam_name')
          ->where('g.hometeam_id', $homeTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(35)
          ->get();

      $shortAway = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.awayteam_id')
          ->select('g.*', 't1.name AS awayteam_name')
          ->where('g.awayteam_id', $awayTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(5)
          ->get();

      $midAway = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.awayteam_id')
          ->select('g.*', 't1.name AS awayteam_name')
          ->where('g.awayteam_id', $awayTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(15)
          ->get();

      $longAway = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.awayteam_id')
          ->select('g.*', 't1.name AS awayteam_name')
          ->where('g.awayteam_id', $awayTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(25)
          ->get();

      $xtraLongAway = DB::table('gamedays AS g')
          ->join('teams AS t1', 't1.id', '=', 'g.awayteam_id')
          ->select('g.*', 't1.name AS awayteam_name')
          ->where('g.awayteam_id', $awayTeam)
          ->whereNotNull('g.winner')
          ->orderBy('date', 'desc')
          ->limit(35)
          ->get();

      $homeTeamName = '';
      $awayTeamName = '';

      $shortTerm = (object) array(
        'Home' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Away' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Total' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        )
      );

      $midTerm = (object) array(
        'Home' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Away' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Total' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        )
      );

      $longTerm = (object) array(
        'Home' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Away' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Total' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        )
      );

      $xtraLongTerm = (object) array(
        'Home' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Away' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        ),
        'Total' => (object)array(
          'Spiele' => 0,
          'Siege' => 0,
          'Niederlagen' => 0,
          'Unentschieden' => 0,
          'SiegPct' => 0,
          'UnentschiedenPct' => 0,
          'NiederlagePct' => 0,
          'TwoWay1x' => 0,
          'TwoWay12' => 0,
          'TwoWayx2' => 0,
          'TwoWay1xPct' => 0,
          'TwoWay12Pct' => 0,
          'TwoWayx2Pct' => 0
        )
      );

      $shortHomeWins = 0;
      $shortHomeDraws = 0;
      $shortHomeLosses = 0;
      $shortHomeGames = 0;

      $midHomeWins = 0;
      $midHomeDraws = 0;
      $midHomeLosses = 0;
      $midHomeGames = 0;

      $longHomeWins = 0;
      $longHomeDraws = 0;
      $longHomeLosses = 0;
      $longHomeGames = 0;

      $xtraLongHomeWins = 0;
      $xtraLongHomeDraws = 0;
      $xtraLongHomeLosses = 0;
      $xtraLongHomeGames = 0;

      $shortAwayWins = 0;
      $shortAwayDraws = 0;
      $shortAwayLosses = 0;
      $shortAwayGames = 0;

      $midAwayWins = 0;
      $midAwayDraws = 0;
      $midAwayLosses = 0;
      $midAwayGames = 0;

      $longAwayWins = 0;
      $longAwayDraws = 0;
      $longAwayLosses = 0;
      $longAwayGames = 0;

      $xtraLongAwayWins = 0;
      $xtraLongAwayDraws = 0;
      $xtraLongAwayLosses = 0;
      $xtraLongAwayGames = 0;

      foreach($shortHome as $entry) {
        $homeTeamName = $entry->hometeam_name;
        if($entry->winner == 'home') {
          $shortHomeWins = $shortHomeWins + 1;
        } else if ($entry->winner == 'draw') {
          $shortHomeDraws = $shortHomeDraws + 1;
        } else if ($entry->winner == 'away') {
          $shortHomeLosses = $shortHomeLosses + 1;
        }

        $shortHomeGames = $shortHomeWins + $shortHomeDraws + $shortHomeLosses;
      }

      $shortTerm->Home->Spiele = $shortHomeGames;
      $shortTerm->Home->Siege = $shortHomeWins;
      $shortTerm->Home->Unentschieden = $shortHomeDraws;
      $shortTerm->Home->Niederlagen = $shortHomeLosses;

      $shortTerm->Home->TwoWay1x = $shortHomeWins + $shortHomeDraws;
      $shortTerm->Home->TwoWay12 = $shortHomeWins + $shortHomeLosses;
      $shortTerm->Home->TwoWayx2 = $shortHomeDraws + $shortHomeLosses;

      $shortTerm->Home->TwoWay1xPct = $shortTerm->Home->TwoWay1x / $shortHomeGames;
      $shortTerm->Home->TwoWay12Pct = $shortTerm->Home->TwoWay12 / $shortHomeGames;
      $shortTerm->Home->TwoWayx2Pct = $shortTerm->Home->TwoWayx2 / $shortHomeGames;

      $shortTerm->Home->SiegPct = $shortHomeWins / $shortHomeGames;
      $shortTerm->Home->UnentschiedenPct = $shortHomeDraws / $shortHomeGames;
      $shortTerm->Home->NiederlagePct = $shortHomeLosses / $shortHomeGames;

      foreach($shortAway as $entry) {
        $awayTeamName = $entry->awayteam_name;
        if($entry->winner == 'away') {
          $shortAwayWins = $shortAwayWins + 1;
        } else if ($entry->winner == 'draw') {
          $shortAwayDraws = $shortAwayDraws + 1;
        } else if ($entry->winner == 'home') {
          $shortAwayLosses = $shortAwayLosses + 1;
        }

        $shortAwayGames = $shortAwayWins + $shortAwayDraws + $shortAwayLosses;
      }

      $shortTerm->Away->Spiele = $shortAwayGames;
      $shortTerm->Away->Siege = $shortAwayWins;
      $shortTerm->Away->Unentschieden = $shortAwayDraws;
      $shortTerm->Away->Niederlagen = $shortAwayLosses;

      $shortTerm->Away->SiegPct = $shortAwayWins / $shortAwayGames;
      $shortTerm->Away->UnentschiedenPct = $shortAwayDraws / $shortAwayGames;
      $shortTerm->Away->NiederlagePct = $shortAwayLosses / $shortAwayGames;

      $shortTerm->Away->TwoWay1x = $shortAwayWins + $shortAwayDraws;
      $shortTerm->Away->TwoWay12 = $shortAwayWins + $shortAwayLosses;
      $shortTerm->Away->TwoWayx2 = $shortAwayDraws + $shortAwayLosses;

      $shortTerm->Away->TwoWay1xPct = $shortTerm->Away->TwoWay1x / $shortAwayGames;
      $shortTerm->Away->TwoWay12Pct = $shortTerm->Away->TwoWay12 / $shortAwayGames;
      $shortTerm->Away->TwoWayx2Pct = $shortTerm->Away->TwoWayx2 / $shortAwayGames;

      $shortTerm->Total->Spiele = $shortHomeGames + $shortAwayGames;
      $shortTerm->Total->Siege = $shortHomeWins + $shortAwayLosses;
      $shortTerm->Total->Unentschieden = $shortHomeDraws + $shortAwayDraws;
      $shortTerm->Total->Niederlagen = $shortHomeLosses + $shortAwayWins;

      $shortTerm->Total->SiegPct = $shortTerm->Total->Siege / $shortTerm->Total->Spiele;
      $shortTerm->Total->UnentschiedenPct = $shortTerm->Total->Unentschieden / $shortTerm->Total->Spiele;
      $shortTerm->Total->NiederlagePct = $shortTerm->Total->Niederlagen / $shortTerm->Total->Spiele;

      $shortTerm->Total->TwoWay1x = $shortTerm->Total->Siege + $shortTerm->Total->Unentschieden;
      $shortTerm->Total->TwoWay12 = $shortTerm->Total->Siege + $shortTerm->Total->Niederlagen;
      $shortTerm->Total->TwoWayx2 = $shortTerm->Total->Unentschieden + $shortTerm->Total->Niederlagen;

      $shortTerm->Total->TwoWay1xPct = $shortTerm->Total->TwoWay1x / $shortTerm->Total->Spiele;
      $shortTerm->Total->TwoWay12Pct = $shortTerm->Total->TwoWay12 / $shortTerm->Total->Spiele;
      $shortTerm->Total->TwoWayx2Pct = $shortTerm->Total->TwoWayx2 / $shortTerm->Total->Spiele;

      foreach($midHome as $entry) {
        if($entry->winner == 'home') {
          $midHomeWins = $midHomeWins + 1;
        } else if ($entry->winner == 'draw') {
          $midHomeDraws = $midHomeDraws + 1;
        } else if ($entry->winner == 'away') {
          $midHomeLosses = $midHomeLosses + 1;
        }

        $midHomeGames = $midHomeWins + $midHomeDraws + $midHomeLosses;
      }

      $midTerm->Home->Spiele = $midHomeGames;
      $midTerm->Home->Siege = $midHomeWins;
      $midTerm->Home->Unentschieden = $midHomeDraws;
      $midTerm->Home->Niederlagen = $midHomeLosses;

      $midTerm->Home->SiegPct = $midHomeWins / $midHomeGames;
      $midTerm->Home->UnentschiedenPct = $midHomeDraws / $midHomeGames;
      $midTerm->Home->NiederlagePct = $midHomeLosses / $midHomeGames;

      $midTerm->Home->TwoWay1x = $midHomeWins + $midHomeDraws;
      $midTerm->Home->TwoWay12 = $midHomeWins + $midHomeLosses;
      $midTerm->Home->TwoWayx2 = $midHomeDraws + $midHomeLosses;

      $midTerm->Home->TwoWay1xPct = $midTerm->Home->TwoWay1x / $midHomeGames;
      $midTerm->Home->TwoWay12Pct = $midTerm->Home->TwoWay12 / $midHomeGames;
      $midTerm->Home->TwoWayx2Pct = $midTerm->Home->TwoWayx2 / $midHomeGames;

      foreach($midAway as $entry) {
        if($entry->winner == 'away') {
          $midAwayWins = $midAwayWins + 1;
        } else if ($entry->winner == 'draw') {
          $midAwayDraws = $midAwayDraws + 1;
        } else if ($entry->winner == 'home') {
          $midAwayLosses = $midAwayLosses + 1;
        }

        $midAwayGames = $midAwayWins + $midAwayDraws + $midAwayLosses;
      }

      $midTerm->Away->Spiele = $midAwayGames;
      $midTerm->Away->Siege = $midAwayWins;
      $midTerm->Away->Unentschieden = $midAwayDraws;
      $midTerm->Away->Niederlagen = $midAwayLosses;

      $midTerm->Away->SiegPct = $midAwayWins / $midAwayGames;
      $midTerm->Away->UnentschiedenPct = $midAwayDraws / $midAwayGames;
      $midTerm->Away->NiederlagePct = $midAwayLosses / $midAwayGames;

      $midTerm->Away->TwoWay1x = $midAwayWins + $midAwayDraws;
      $midTerm->Away->TwoWay12 = $midAwayWins + $midAwayLosses;
      $midTerm->Away->TwoWayx2 = $midAwayDraws + $midAwayLosses;

      $midTerm->Away->TwoWay1xPct = $midTerm->Away->TwoWay1x / $midAwayGames;
      $midTerm->Away->TwoWay12Pct = $midTerm->Away->TwoWay12 / $midAwayGames;
      $midTerm->Away->TwoWayx2Pct = $midTerm->Away->TwoWayx2 / $midAwayGames;

      $midTerm->Total->Spiele = $midHomeGames + $midAwayGames;
      $midTerm->Total->Siege = $midHomeWins + $midAwayLosses;
      $midTerm->Total->Unentschieden = $midHomeDraws + $midAwayDraws;
      $midTerm->Total->Niederlagen = $midHomeLosses + $midAwayWins;

      $midTerm->Total->SiegPct = $midTerm->Total->Siege / $midTerm->Total->Spiele;
      $midTerm->Total->UnentschiedenPct = $midTerm->Total->Unentschieden / $midTerm->Total->Spiele;
      $midTerm->Total->NiederlagePct = $midTerm->Total->Niederlagen / $midTerm->Total->Spiele;

      $midTerm->Total->TwoWay1x = $midTerm->Total->Siege + $midTerm->Total->Unentschieden;
      $midTerm->Total->TwoWay12 = $midTerm->Total->Siege + $midTerm->Total->Niederlagen;
      $midTerm->Total->TwoWayx2 = $midTerm->Total->Unentschieden + $midTerm->Total->Niederlagen;

      $midTerm->Total->TwoWay1xPct = $midTerm->Total->TwoWay1x / $midTerm->Total->Spiele;
      $midTerm->Total->TwoWay12Pct = $midTerm->Total->TwoWay12 / $midTerm->Total->Spiele;
      $midTerm->Total->TwoWayx2Pct = $midTerm->Total->TwoWayx2 / $midTerm->Total->Spiele;

      foreach($longHome as $entry) {
        if($entry->winner == 'home') {
          $longHomeWins = $longHomeWins + 1;
        } else if ($entry->winner == 'draw') {
          $longHomeDraws = $longHomeDraws + 1;
        } else if ($entry->winner == 'away') {
          $longHomeLosses = $longHomeLosses + 1;
        }

        $longHomeGames = $longHomeWins + $longHomeDraws + $longHomeLosses;
      }

      $longTerm->Home->Spiele = $longHomeGames;
      $longTerm->Home->Siege = $longHomeWins;
      $longTerm->Home->Unentschieden = $longHomeDraws;
      $longTerm->Home->Niederlagen = $longHomeLosses;

      $longTerm->Home->SiegPct = $longHomeWins / $longHomeGames;
      $longTerm->Home->UnentschiedenPct = $longHomeDraws / $longHomeGames;
      $longTerm->Home->NiederlagePct = $longHomeLosses / $longHomeGames;

      $longTerm->Home->TwoWay1x = $longHomeWins + $longHomeDraws;
      $longTerm->Home->TwoWay12 = $longHomeWins + $longHomeLosses;
      $longTerm->Home->TwoWayx2 = $longHomeDraws + $longHomeLosses;

      $longTerm->Home->TwoWay1xPct = $longTerm->Home->TwoWay1x / $longHomeGames;
      $longTerm->Home->TwoWay12Pct = $longTerm->Home->TwoWay12 / $longHomeGames;
      $longTerm->Home->TwoWayx2Pct = $longTerm->Home->TwoWayx2 / $longHomeGames;

      foreach($longAway as $entry) {
        if($entry->winner == 'away') {
          $longAwayWins = $longAwayWins + 1;
        } else if ($entry->winner == 'draw') {
          $longAwayDraws = $longAwayDraws + 1;
        } else if ($entry->winner == 'home') {
          $longAwayLosses = $longAwayLosses + 1;
        }

        $longAwayGames = $longAwayWins + $longAwayDraws + $longAwayLosses;
      }

      $longTerm->Away->Spiele = $longAwayGames;
      $longTerm->Away->Siege = $longAwayWins;
      $longTerm->Away->Unentschieden = $longAwayDraws;
      $longTerm->Away->Niederlagen = $longAwayLosses;

      $longTerm->Away->SiegPct = $longAwayWins / $longAwayGames;
      $longTerm->Away->UnentschiedenPct = $longAwayDraws / $longAwayGames;
      $longTerm->Away->NiederlagePct = $longAwayLosses / $longAwayGames;

      $longTerm->Away->TwoWay1x = $longAwayWins + $longAwayDraws;
      $longTerm->Away->TwoWay12 = $longAwayWins + $longAwayLosses;
      $longTerm->Away->TwoWayx2 = $longAwayDraws + $longAwayLosses;

      $longTerm->Away->TwoWay1xPct = $longTerm->Away->TwoWay1x / $longAwayGames;
      $longTerm->Away->TwoWay12Pct = $longTerm->Away->TwoWay12 / $longAwayGames;
      $longTerm->Away->TwoWayx2Pct = $longTerm->Away->TwoWayx2 / $longAwayGames;

      $longTerm->Total->Spiele = $longHomeGames + $longAwayGames;
      $longTerm->Total->Siege = $longHomeWins + $longAwayLosses;
      $longTerm->Total->Unentschieden = $longHomeDraws + $longAwayDraws;
      $longTerm->Total->Niederlagen = $longHomeLosses + $longAwayWins;

      $longTerm->Total->SiegPct = $longTerm->Total->Siege / $longTerm->Total->Spiele;
      $longTerm->Total->UnentschiedenPct = $longTerm->Total->Unentschieden / $longTerm->Total->Spiele;
      $longTerm->Total->NiederlagePct = $longTerm->Total->Niederlagen / $longTerm->Total->Spiele;

      $longTerm->Total->TwoWay1x = $longTerm->Total->Siege + $longTerm->Total->Unentschieden;
      $longTerm->Total->TwoWay12 = $longTerm->Total->Siege + $longTerm->Total->Niederlagen;
      $longTerm->Total->TwoWayx2 = $longTerm->Total->Unentschieden + $longTerm->Total->Niederlagen;

      $longTerm->Total->TwoWay1xPct = $longTerm->Total->TwoWay1x / $longTerm->Total->Spiele;
      $longTerm->Total->TwoWay12Pct = $longTerm->Total->TwoWay12 / $longTerm->Total->Spiele;
      $longTerm->Total->TwoWayx2Pct = $longTerm->Total->TwoWayx2 / $longTerm->Total->Spiele;

      foreach($xtraLongHome as $entry) {
        if($entry->winner == 'home') {
          $xtraLongHomeWins = $xtraLongHomeWins + 1;
        } else if ($entry->winner == 'draw') {
          $xtraLongHomeDraws = $xtraLongHomeDraws + 1;
        } else if ($entry->winner == 'away') {
          $xtraLongHomeLosses = $xtraLongHomeLosses + 1;
        }

        $xtraLongHomeGames = $xtraLongHomeWins + $xtraLongHomeDraws + $xtraLongHomeLosses;
      }

      $xtraLongTerm->Home->Spiele = $xtraLongHomeGames;
      $xtraLongTerm->Home->Siege = $xtraLongHomeWins;
      $xtraLongTerm->Home->Unentschieden = $xtraLongHomeDraws;
      $xtraLongTerm->Home->Niederlagen = $xtraLongHomeLosses;

      $xtraLongTerm->Home->SiegPct = $xtraLongHomeWins / $xtraLongHomeGames;
      $xtraLongTerm->Home->UnentschiedenPct = $xtraLongHomeDraws / $xtraLongHomeGames;
      $xtraLongTerm->Home->NiederlagePct = $xtraLongHomeLosses / $xtraLongHomeGames;

      $xtraLongTerm->Home->TwoWay1x = $xtraLongHomeWins + $xtraLongHomeDraws;
      $xtraLongTerm->Home->TwoWay12 = $xtraLongHomeWins + $xtraLongHomeLosses;
      $xtraLongTerm->Home->TwoWayx2 = $xtraLongHomeDraws + $xtraLongHomeLosses;

      $xtraLongTerm->Home->TwoWay1xPct = $xtraLongTerm->Home->TwoWay1x / $xtraLongHomeGames;
      $xtraLongTerm->Home->TwoWay12Pct = $xtraLongTerm->Home->TwoWay12 / $xtraLongHomeGames;
      $xtraLongTerm->Home->TwoWayx2Pct = $xtraLongTerm->Home->TwoWayx2 / $xtraLongHomeGames;

      foreach($xtraLongAway as $entry) {
        if($entry->winner == 'away') {
          $xtraLongAwayWins = $xtraLongAwayWins + 1;
        } else if ($entry->winner == 'draw') {
          $xtraLongAwayDraws = $xtraLongAwayDraws + 1;
        } else if ($entry->winner == 'home') {
          $xtraLongAwayLosses = $xtraLongAwayLosses + 1;
        }

        $xtraLongAwayGames = $xtraLongAwayWins + $xtraLongAwayDraws + $xtraLongAwayLosses;
      }

      $xtraLongTerm->Away->Spiele = $xtraLongAwayGames;
      $xtraLongTerm->Away->Siege = $xtraLongAwayWins;
      $xtraLongTerm->Away->Unentschieden = $xtraLongAwayDraws;
      $xtraLongTerm->Away->Niederlagen = $xtraLongAwayLosses;

      $xtraLongTerm->Away->SiegPct = $xtraLongAwayWins / $xtraLongAwayGames;
      $xtraLongTerm->Away->UnentschiedenPct = $xtraLongAwayDraws / $xtraLongAwayGames;
      $xtraLongTerm->Away->NiederlagePct = $xtraLongAwayLosses / $xtraLongAwayGames;

      $xtraLongTerm->Away->TwoWay1x = $xtraLongAwayWins + $xtraLongAwayDraws;
      $xtraLongTerm->Away->TwoWay12 = $xtraLongAwayWins + $xtraLongAwayLosses;
      $xtraLongTerm->Away->TwoWayx2 = $xtraLongAwayDraws + $xtraLongAwayLosses;

      $xtraLongTerm->Away->TwoWay1xPct = $xtraLongTerm->Away->TwoWay1x / $xtraLongAwayGames;
      $xtraLongTerm->Away->TwoWay12Pct = $xtraLongTerm->Away->TwoWay12 / $xtraLongAwayGames;
      $xtraLongTerm->Away->TwoWayx2Pct = $xtraLongTerm->Away->TwoWayx2 / $xtraLongAwayGames;

      $xtraLongTerm->Total->Spiele = $xtraLongHomeGames + $xtraLongAwayGames;
      $xtraLongTerm->Total->Siege = $xtraLongHomeWins + $xtraLongAwayLosses;
      $xtraLongTerm->Total->Unentschieden = $xtraLongHomeDraws + $xtraLongAwayDraws;
      $xtraLongTerm->Total->Niederlagen = $xtraLongHomeLosses + $xtraLongAwayWins;

      $xtraLongTerm->Total->SiegPct = $xtraLongTerm->Total->Siege / $xtraLongTerm->Total->Spiele;
      $xtraLongTerm->Total->UnentschiedenPct = $xtraLongTerm->Total->Unentschieden / $xtraLongTerm->Total->Spiele;
      $xtraLongTerm->Total->NiederlagePct = $xtraLongTerm->Total->Niederlagen / $xtraLongTerm->Total->Spiele;

      $xtraLongTerm->Total->TwoWay1x = $xtraLongTerm->Total->Siege + $xtraLongTerm->Total->Unentschieden;
      $xtraLongTerm->Total->TwoWay12 = $xtraLongTerm->Total->Siege + $xtraLongTerm->Total->Niederlagen;
      $xtraLongTerm->Total->TwoWayx2 = $xtraLongTerm->Total->Unentschieden + $xtraLongTerm->Total->Niederlagen;

      $xtraLongTerm->Total->TwoWay1xPct = $xtraLongTerm->Total->TwoWay1x / $xtraLongTerm->Total->Spiele;
      $xtraLongTerm->Total->TwoWay12Pct = $xtraLongTerm->Total->TwoWay12 / $xtraLongTerm->Total->Spiele;
      $xtraLongTerm->Total->TwoWayx2Pct = $xtraLongTerm->Total->TwoWayx2 / $xtraLongTerm->Total->Spiele;

      $returnStats = (object) array(
        'HomeTeamName' => $homeTeamName,
        'HomeTeamId' => $homeTeam,
        'AwayTeamName' => $awayTeamName,
        'AwayTeamId' => $awayTeam,
        'ShortTerm' => $shortTerm,
        'MidTerm' => $midTerm,
        'LongTerm' => $longTerm,
        'XtraLongTerm' => $xtraLongTerm
      );

      return response()->json(['Stats' => $returnStats]);
    }
}
