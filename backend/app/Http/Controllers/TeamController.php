<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class TeamController extends Controller
{
  /**
  * Get Teams
  *
  * @return Response
  */
    public function getTeams()
    {
      $teams = DB::table('teams')->get();

      return response()->json(['Teams' => $teams]);
    }
}
