INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2016-04-30',36,60,64,2,1,'home',4,2016),
('2016-04-30',36,89,59,1,0,'home',4,2016),
('2016-04-30',36,71,73,1,1,'draw',4,2016),
('2016-04-30',36,72,100,3,2,'home',4,2016),
('2016-04-30',36,69,76,0,3,'away',4,2016),
('2016-04-30',36,65,91,1,0,'home',4,2016),
('2016-05-01',36,68,75,3,1,'home',4,2016),
('2016-05-01',36,74,67,1,1,'draw',4,2016),
('2016-05-01',36,62,63,4,2,'home',4,2016),
('2016-05-02',36,66,70,2,2,'draw',4,2016),
('2016-05-07',37,91,74,0,1,'away',4,2016),
('2016-05-07',37,100,89,0,0,'draw',4,2016),
('2016-05-07',37,64,69,1,1,'draw',4,2016),
('2016-05-07',37,59,71,2,1,'home',4,2016),
('2016-05-07',37,73,66,3,2,'home',4,2016),
('2016-05-07',37,76,68,1,4,'away',4,2016),
('2016-05-07',37,67,60,3,1,'home',4,2016),
('2016-05-08',37,70,62,1,2,'away',4,2016),
('2016-05-08',37,75,72,2,0,'home',4,2016),
('2016-05-08',37,63,65,2,2,'draw',4,2016),
('2016-05-15',38,65,100,4,0,'home',4,2016),
('2016-05-15',38,66,67,1,1,'draw',4,2016),
('2016-05-15',38,60,91,3,0,'home',4,2016),
('2016-05-15',38,89,70,5,1,'home',4,2016),
('2016-05-15',38,62,59,4,1,'home',4,2016),
('2016-05-15',38,71,76,2,1,'home',4,2016),
('2016-05-15',38,68,63,1,1,'draw',4,2016),
('2016-05-15',38,72,73,2,2,'draw',4,2016),
('2016-05-15',38,69,75,1,1,'draw',4,2016),
('2016-05-17',38,74,64,3,1,'home',4,2016);