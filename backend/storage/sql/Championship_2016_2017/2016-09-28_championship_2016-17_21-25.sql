INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2016-12-13',21,78,83,NULL,NULL,NULL,5,2017),
('2016-12-13',21,79,93,NULL,NULL,NULL,5,2017),
('2016-12-13',21,80,94,NULL,NULL,NULL,5,2017),
('2016-12-13',21,96,82,NULL,NULL,NULL,5,2017),
('2016-12-13',21,90,98,NULL,NULL,NULL,5,2017),
('2016-12-13',21,77,86,NULL,NULL,NULL,5,2017),
('2016-12-13',21,99,85,NULL,NULL,NULL,5,2017),
('2016-12-13',21,91,100,NULL,NULL,NULL,5,2017),
('2016-12-13',21,84,97,NULL,NULL,NULL,5,2017),
('2016-12-13',21,87,81,NULL,NULL,NULL,5,2017),
('2016-12-13',21,88,95,NULL,NULL,NULL,5,2017),
('2016-12-13',21,92,89,NULL,NULL,NULL,5,2017),
('2016-12-17',22,78,93,NULL,NULL,NULL,5,2017),
('2016-12-17',22,79,85,NULL,NULL,NULL,5,2017),
('2016-12-17',22,80,97,NULL,NULL,NULL,5,2017),
('2016-12-17',22,96,89,NULL,NULL,NULL,5,2017),
('2016-12-17',22,90,95,NULL,NULL,NULL,5,2017),
('2016-12-17',22,77,81,NULL,NULL,NULL,5,2017),
('2016-12-17',22,99,94,NULL,NULL,NULL,5,2017),
('2016-12-17',22,91,82,NULL,NULL,NULL,5,2017),
('2016-12-17',22,84,98,NULL,NULL,NULL,5,2017),
('2016-12-17',22,87,100,NULL,NULL,NULL,5,2017),
('2016-12-17',22,88,86,NULL,NULL,NULL,5,2017),
('2016-12-17',22,92,83,NULL,NULL,NULL,5,2017),
('2016-12-26',23,100,96,NULL,NULL,NULL,5,2017),
('2016-12-26',23,95,79,NULL,NULL,NULL,5,2017),
('2016-12-26',23,94,90,NULL,NULL,NULL,5,2017),
('2016-12-26',23,93,87,NULL,NULL,NULL,5,2017),
('2016-12-26',23,81,78,NULL,NULL,NULL,5,2017),
('2016-12-26',23,82,84,NULL,NULL,NULL,5,2017),
('2016-12-26',23,83,77,NULL,NULL,NULL,5,2017),
('2016-12-26',23,89,88,NULL,NULL,NULL,5,2017),
('2016-12-26',23,97,99,NULL,NULL,NULL,5,2017),
('2016-12-26',23,85,91,NULL,NULL,NULL,5,2017),
('2016-12-26',23,86,92,NULL,NULL,NULL,5,2017),
('2016-12-26',23,98,80,NULL,NULL,NULL,5,2017),
('2016-12-31',24,100,99,NULL,NULL,NULL,5,2017),
('2016-12-31',24,95,78,NULL,NULL,NULL,5,2017),
('2016-12-31',24,94,91,NULL,NULL,NULL,5,2017),
('2016-12-31',24,93,90,NULL,NULL,NULL,5,2017),
('2016-12-31',24,81,92,NULL,NULL,NULL,5,2017),
('2016-12-31',24,82,79,NULL,NULL,NULL,5,2017),
('2016-12-31',24,83,80,NULL,NULL,NULL,5,2017),
('2016-12-31',24,89,84,NULL,NULL,NULL,5,2017),
('2016-12-31',24,97,88,NULL,NULL,NULL,5,2017),
('2016-12-31',24,85,77,NULL,NULL,NULL,5,2017),
('2016-12-31',24,86,96,NULL,NULL,NULL,5,2017),
('2016-12-31',24,98,87,NULL,NULL,NULL,5,2017),
('2017-01-02',25,78,94,NULL,NULL,NULL,5,2017),
('2017-01-02',25,79,89,NULL,NULL,NULL,5,2017),
('2017-01-02',25,80,85,NULL,NULL,NULL,5,2017),
('2017-01-02',25,96,97,NULL,NULL,NULL,5,2017),
('2017-01-02',25,90,100,NULL,NULL,NULL,5,2017),
('2017-01-02',25,77,93,NULL,NULL,NULL,5,2017),
('2017-01-02',25,99,86,NULL,NULL,NULL,5,2017),
('2017-01-02',25,91,81,NULL,NULL,NULL,5,2017),
('2017-01-02',25,84,95,NULL,NULL,NULL,5,2017),
('2017-01-02',25,87,83,NULL,NULL,NULL,5,2017),
('2017-01-02',25,88,98,NULL,NULL,NULL,5,2017),
('2017-01-02',25,92,82,NULL,NULL,NULL,5,2017);