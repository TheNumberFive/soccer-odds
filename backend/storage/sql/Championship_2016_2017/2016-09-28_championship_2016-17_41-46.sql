INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2017-04-08',41,78,81,NULL,NULL,NULL,5,2017),
('2017-04-08',41,79,95,NULL,NULL,NULL,5,2017),
('2017-04-08',41,80,98,NULL,NULL,NULL,5,2017),
('2017-04-08',41,96,100,NULL,NULL,NULL,5,2017),
('2017-04-08',41,90,94,NULL,NULL,NULL,5,2017),
('2017-04-08',41,77,83,NULL,NULL,NULL,5,2017),
('2017-04-08',41,99,97,NULL,NULL,NULL,5,2017),
('2017-04-08',41,91,85,NULL,NULL,NULL,5,2017),
('2017-04-08',41,84,82,NULL,NULL,NULL,5,2017),
('2017-04-08',41,87,93,NULL,NULL,NULL,5,2017),
('2017-04-08',41,88,89,NULL,NULL,NULL,5,2017),
('2017-04-08',41,92,86,NULL,NULL,NULL,5,2017),
('2017-04-15',42,100,85,NULL,NULL,NULL,5,2017),
('2017-04-15',42,94,81,NULL,NULL,NULL,5,2017),
('2017-04-15',42,80,87,NULL,NULL,NULL,5,2017),
('2017-04-15',42,96,83,NULL,NULL,NULL,5,2017),
('2017-04-15',42,82,97,NULL,NULL,NULL,5,2017),
('2017-04-15',42,89,99,NULL,NULL,NULL,5,2017),
('2017-04-15',42,91,77,NULL,NULL,NULL,5,2017),
('2017-04-15',42,84,79,NULL,NULL,NULL,5,2017),
('2017-04-15',42,86,78,NULL,NULL,NULL,5,2017),
('2017-04-15',42,88,90,NULL,NULL,NULL,5,2017),
('2017-04-15',42,92,95,NULL,NULL,NULL,5,2017),
('2017-04-15',42,98,93,NULL,NULL,NULL,5,2017),
('2017-04-17',43,95,94,NULL,NULL,NULL,5,2017),
('2017-04-17',43,78,96,NULL,NULL,NULL,5,2017),
('2017-04-17',43,79,80,NULL,NULL,NULL,5,2017),
('2017-04-17',43,93,92,NULL,NULL,NULL,5,2017),
('2017-04-17',43,90,84,NULL,NULL,NULL,5,2017),
('2017-04-17',43,81,82,NULL,NULL,NULL,5,2017),
('2017-04-17',43,77,100,NULL,NULL,NULL,5,2017),
('2017-04-17',43,83,89,NULL,NULL,NULL,5,2017),
('2017-04-17',43,99,98,NULL,NULL,NULL,5,2017),
('2017-04-17',43,97,91,NULL,NULL,NULL,5,2017),
('2017-04-17',43,87,88,NULL,NULL,NULL,5,2017),
('2017-04-17',43,85,86,NULL,NULL,NULL,5,2017),
('2017-04-22',44,100,78,NULL,NULL,NULL,5,2017),
('2017-04-22',44,94,87,NULL,NULL,NULL,5,2017),
('2017-04-22',44,80,95,NULL,NULL,NULL,5,2017),
('2017-04-22',44,96,99,NULL,NULL,NULL,5,2017),
('2017-04-22',44,82,77,NULL,NULL,NULL,5,2017),
('2017-04-22',44,89,97,NULL,NULL,NULL,5,2017),
('2017-04-22',44,91,93,NULL,NULL,NULL,5,2017),
('2017-04-22',44,84,85,NULL,NULL,NULL,5,2017),
('2017-04-22',44,86,83,NULL,NULL,NULL,5,2017),
('2017-04-22',44,88,81,NULL,NULL,NULL,5,2017),
('2017-04-22',44,92,90,NULL,NULL,NULL,5,2017),
('2017-04-22',44,98,79,NULL,NULL,NULL,5,2017),
('2017-04-29',45,95,96,NULL,NULL,NULL,5,2017),
('2017-04-29',45,78,82,NULL,NULL,NULL,5,2017),
('2017-04-29',45,79,100,NULL,NULL,NULL,5,2017),
('2017-04-29',45,93,80,NULL,NULL,NULL,5,2017),
('2017-04-29',45,90,89,NULL,NULL,NULL,5,2017),
('2017-04-29',45,81,98,NULL,NULL,NULL,5,2017),
('2017-04-29',45,77,94,NULL,NULL,NULL,5,2017),
('2017-04-29',45,83,88,NULL,NULL,NULL,5,2017),
('2017-04-29',45,99,91,NULL,NULL,NULL,5,2017),
('2017-04-29',45,97,86,NULL,NULL,NULL,5,2017),
('2017-04-29',45,87,84,NULL,NULL,NULL,5,2017),
('2017-04-29',45,85,92,NULL,NULL,NULL,5,2017),
('2017-05-07',46,100,93,NULL,NULL,NULL,5,2017),
('2017-05-07',46,94,79,NULL,NULL,NULL,5,2017),
('2017-05-07',46,80,78,NULL,NULL,NULL,5,2017),
('2017-05-07',46,96,85,NULL,NULL,NULL,5,2017),
('2017-05-07',46,82,90,NULL,NULL,NULL,5,2017),
('2017-05-07',46,89,95,NULL,NULL,NULL,5,2017),
('2017-05-07',46,91,87,NULL,NULL,NULL,5,2017),
('2017-05-07',46,84,83,NULL,NULL,NULL,5,2017),
('2017-05-07',46,86,81,NULL,NULL,NULL,5,2017),
('2017-05-07',46,88,77,NULL,NULL,NULL,5,2017),
('2017-05-07',46,92,99,NULL,NULL,NULL,5,2017),
('2017-05-07',46,98,97,NULL,NULL,NULL,5,2017);