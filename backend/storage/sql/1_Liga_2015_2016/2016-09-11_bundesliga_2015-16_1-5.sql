INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2015-08-14',1,1,14,5,0,'home',1,2016),
('2015-08-15',1,6,17,2,1,'home',1,2016),
('2015-08-15',1,9,5,0,1,'away',1,2016),
('2015-08-15',1,7,3,0,3,'away',1,2016),
('2015-08-15',1,10,12,0,1,'away',1,2016),
('2015-08-15',1,15,33,2,2,'draw',1,2016),
('2015-08-15',1,2,4,4,0,'home',1,2016),
('2015-08-16',1,8,16,2,1,'home',1,2016),
('2015-08-16',1,36,13,1,3,'away',1,2016),
('2015-08-21',2,5,7,1,1,'draw',1,2016),
('2015-08-22',2,3,15,1,1,'draw',1,2016),
('2015-08-22',2,16,9,1,1,'draw',1,2016),
('2015-08-22',2,13,8,1,1,'draw',1,2016),
('2015-08-22',2,33,6,0,1,'away',1,2016),
('2015-08-22',2,17,1,1,2,'away',1,2016),
('2015-08-22',2,14,36,3,2,'home',1,2016),
('2015-08-23',2,12,2,0,4,'away',1,2016),
('2015-08-23',2,4,10,1,2,'away',1,2016),
('2015-08-28',3,8,3,3,0,'home',1,2016),
('2015-08-29',3,36,16,1,4,'away',1,2016),
('2015-08-29',3,9,12,0,1,'away',1,2016),
('2015-08-29',3,13,14,2,1,'home',1,2016),
('2015-08-29',3,10,33,3,0,'home',1,2016),
('2015-08-29',3,15,17,0,0,'draw',1,2016),
('2015-08-29',3,1,6,3,0,'home',1,2016),
('2015-08-30',3,2,5,3,1,'home',1,2016),
('2015-08-30',3,7,4,2,1,'home',1,2016),
('2015-09-11',4,4,14,0,3,'away',1,2016),
('2015-09-12',4,1,9,2,1,'home',1,2016),
('2015-09-12',4,6,15,0,1,'away',1,2016),
('2015-09-12',4,33,2,2,4,'away',1,2016),
('2015-09-12',4,5,36,2,1,'home',1,2016),
('2015-09-12',4,12,8,0,0,'draw',1,2016),
('2015-09-12',4,16,13,6,2,'home',1,2016),
('2015-09-13',4,17,7,1,3,'away',1,2016),
('2015-09-13',4,3,10,2,1,'home',1,2016),
('2015-09-18',5,10,17,3,1,'home',1,2016),
('2015-09-19',5,8,5,2,0,'home',1,2016),
('2015-09-19',5,14,16,0,0,'draw',1,2016),
('2015-09-19',5,7,12,0,1,'away',1,2016),
('2015-09-19',5,13,4,1,0,'home',1,2016),
('2015-09-19',5,15,1,0,3,'away',1,2016),
('2015-09-20',5,36,3,0,1,'away',1,2016),
('2015-09-20',5,2,6,3,0,'home',1,2016),
('2015-09-20',5,9,33,2,0,'home',1,2016);
