INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2015-12-11',16,10,36,0,0,'draw',1,2016),
('2015-12-12',16,1,12,2,0,'home',1,2016),
('2015-12-12',16,8,14,1,1,'draw',1,2016),
('2015-12-12',16,7,13,1,1,'draw',1,2016),
('2015-12-12',16,17,33,1,0,'home',1,2016),
('2015-12-12',16,15,5,0,4,'away',1,2016),
('2015-12-12',16,6,4,5,0,'home',1,2016),
('2015-12-13',16,9,3,2,1,'home',1,2016),
('2015-12-13',16,2,16,4,1,'home',1,2016),
('2015-12-18',17,3,17,1,0,'home',1,2016),
('2015-12-19',17,14,9,0,1,'away',1,2016),
('2015-12-19',17,16,7,2,1,'home',1,2016),
('2015-12-19',17,13,2,2,1,'home',1,2016),
('2015-12-19',17,33,1,0,1,'away',1,2016),
('2015-12-19',17,12,6,0,1,'away',1,2016),
('2015-12-19',17,36,8,3,1,'home',1,2016),
('2015-12-20',17,5,10,2,0,'home',1,2016),
('2015-12-20',17,4,15,3,2,'home',1,2016),
('2016-01-22',18,14,1,1,2,'away',1,2016),
('2016-01-23',18,13,36,1,3,'away',1,2016),
('2016-01-23',18,33,15,1,2,'away',1,2016),
('2016-01-23',18,5,9,0,0,'draw',1,2016),
('2016-01-23',18,17,6,1,1,'draw',1,2016),
('2016-01-23',18,12,10,1,0,'home',1,2016),
('2016-01-23',18,4,2,1,3,'away',1,2016),
('2016-01-24',18,16,8,3,2,'home',1,2016),
('2016-01-24',18,3,7,1,3,'away',1,2016),
('2016-01-29',19,10,4,1,0,'home',1,2016),
('2016-01-30',19,2,12,2,0,'home',1,2016),
('2016-01-30',19,6,33,3,0,'home',1,2016),
('2016-01-30',19,9,16,0,0,'draw',1,2016),
('2016-01-30',19,7,5,3,3,'draw',1,2016),
('2016-01-30',19,15,3,0,2,'away',1,2016),
('2016-01-30',19,36,14,2,1,'home',1,2016),
('2016-01-31',19,8,13,1,1,'draw',1,2016),
('2016-01-31',19,1,17,2,0,'home',1,2016),
('2016-02-05',20,4,7,5,1,'home',1,2016),
('2016-02-06',20,3,8,3,0,'home',1,2016),
('2016-02-06',20,16,36,2,4,'away',1,2016),
('2016-02-06',20,33,10,0,1,'away',1,2016),
('2016-02-06',20,5,2,0,0,'draw',1,2016),
('2016-02-06',20,12,9,2,1,'home',1,2016),
('2016-02-06',20,6,1,0,0,'draw',1,2016),
('2016-02-07',20,14,13,1,1,'draw',1,2016),
('2016-02-07',20,17,15,0,2,'away',1,2016);
