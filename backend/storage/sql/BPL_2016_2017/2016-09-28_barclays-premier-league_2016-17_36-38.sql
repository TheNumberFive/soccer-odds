INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2017-05-06',36,64,71,NULL,NULL,NULL,4,2017),
('2017-05-06',36,65,74,NULL,NULL,NULL,4,2017),
('2017-05-06',36,58,69,NULL,NULL,NULL,4,2017),
('2017-05-06',36,66,61,NULL,NULL,NULL,4,2017),
('2017-05-06',36,57,73,NULL,NULL,NULL,4,2017),
('2017-05-06',36,67,72,NULL,NULL,NULL,4,2017),
('2017-05-06',36,75,62,NULL,NULL,NULL,4,2017),
('2017-05-06',36,63,59,NULL,NULL,NULL,4,2017),
('2017-05-06',36,68,60,NULL,NULL,NULL,4,2017),
('2017-05-06',36,76,70,NULL,NULL,NULL,4,2017),
('2017-05-13',37,64,58,NULL,NULL,NULL,4,2017),
('2017-05-13',37,59,57,NULL,NULL,NULL,4,2017),
('2017-05-13',37,60,72,NULL,NULL,NULL,4,2017),
('2017-05-13',37,63,67,NULL,NULL,NULL,4,2017),
('2017-05-13',37,61,62,NULL,NULL,NULL,4,2017),
('2017-05-13',37,71,65,NULL,NULL,NULL,4,2017),
('2017-05-13',37,73,68,NULL,NULL,NULL,4,2017),
('2017-05-13',37,70,74,NULL,NULL,NULL,4,2017),
('2017-05-13',37,69,66,NULL,NULL,NULL,4,2017),
('2017-05-13',37,76,75,NULL,NULL,NULL,4,2017),
('2017-05-21',38,65,60,NULL,NULL,NULL,4,2017),
('2017-05-21',38,58,76,NULL,NULL,NULL,4,2017),
('2017-05-21',38,66,73,NULL,NULL,NULL,4,2017),
('2017-05-21',38,57,70,NULL,NULL,NULL,4,2017),
('2017-05-21',38,67,64,NULL,NULL,NULL,4,2017),
('2017-05-21',38,75,61,NULL,NULL,NULL,4,2017),
('2017-05-21',38,74,59,NULL,NULL,NULL,4,2017),
('2017-05-21',38,62,71,NULL,NULL,NULL,4,2017),
('2017-05-21',38,68,69,NULL,NULL,NULL,4,2017),
('2017-05-21',38,72,63,NULL,NULL,NULL,4,2017);