INSERT INTO `tipico`.`gamedays` (`date`,`gameday`, `hometeam_id`, `awayteam_id`, `hometeam_goals`, `awayteam_goals`, `winner`, `league_id`, `season`) VALUES 
('2016-08-13',1,57,67,2,1,'home',4,2017),
('2016-08-13',1,58,68,0,1,'away',4,2017),
('2016-08-13',1,59,69,0,1,'away',4,2017),
('2016-08-13',1,60,70,1,1,'draw',4,2017),
('2016-08-13',1,61,71,1,1,'draw',4,2017),
('2016-08-13',1,62,72,1,1,'draw',4,2017),
('2016-08-13',1,63,73,2,1,'home',4,2017),
('2016-08-14',1,64,74,1,3,'away',4,2017),
('2016-08-14',1,65,75,3,4,'away',4,2017),
('2016-08-15',1,66,76,2,1,'home',4,2017),
('2016-08-19',2,74,62,2,0,'home',4,2017),
('2016-08-20',2,71,63,1,4,'away',4,2017),
('2016-08-20',2,58,75,2,0,'home',4,2017),
('2016-08-20',2,68,57,0,2,'away',4,2017),
('2016-08-20',2,70,59,1,0,'home',4,2017),
('2016-08-20',2,72,66,1,2,'away',4,2017),
('2016-08-20',2,69,60,1,2,'away',4,2017),
('2016-08-20',2,67,65,0,0,'draw',4,2017),
('2016-08-21',2,73,61,1,2,'away',4,2017),
('2016-08-21',2,76,64,1,0,'home',4,2017),
('2016-08-27',3,70,75,1,1,'draw',4,2017),
('2016-08-27',3,66,58,3,0,'home',4,2017),
('2016-08-27',3,59,64,1,1,'draw',4,2017),
('2016-08-27',3,60,71,1,0,'home',4,2017),
('2016-08-27',3,67,68,2,1,'home',4,2017),
('2016-08-27',3,62,73,1,1,'draw',4,2017),
('2016-08-27',3,72,65,1,3,'away',4,2017),
('2016-08-27',3,57,74,0,1,'away',4,2017),
('2016-08-28',3,69,61,0,0,'draw',4,2017),
('2016-08-28',3,63,76,3,1,'home',4,2017),
('2016-09-10',4,74,63,1,2,'away',4,2017),
('2016-09-10',4,64,69,1,0,'home',4,2017),
('2016-09-10',4,65,62,2,1,'home',4,2017),
('2016-09-10',4,58,57,1,1,'draw',4,2017),
('2016-09-10',4,61,59,1,2,'away',4,2017),
('2016-09-10',4,71,70,0,4,'away',4,2017),
('2016-09-10',4,76,72,2,4,'away',4,2017),
('2016-09-10',4,75,67,4,1,'home',4,2017),
('2016-09-11',4,68,66,2,2,'draw',4,2017),
('2016-09-12',4,73,60,0,3,'away',4,2017),
('2016-09-16',5,66,75,1,2,'away',4,2017),
('2016-09-17',5,57,65,1,4,'away',4,2017),
('2016-09-17',5,67,58,3,0,'home',4,2017),
('2016-09-17',5,63,64,4,0,'home',4,2017),
('2016-09-17',5,69,76,4,2,'home',4,2017),
('2016-09-17',5,60,61,3,1,'home',4,2017),
('2016-09-18',5,72,74,3,1,'home',4,2017),
('2016-09-18',5,59,71,4,1,'home',4,2017),
('2016-09-18',5,62,68,1,0,'home',4,2017),
('2016-09-18',5,70,73,1,0,'home',4,2017);
